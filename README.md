**Delphi AutoUpdate**

Componente para Delphi criado por Ricardo Alves Carvalho que permite rapida atualização de seus aplicativos via FTP.

Propriedades a serem utilizadas:


AutoNeedVersion=se True, o componente ira fazer a verificação automática se a versão do aplicativo é maior ou menor que a versão contida no servidor e automaticamente ira fazer o update ou deploy.
FTPHost= nome ou ip do servidor onde o FTP esta configurado, por exemplo FTPHost=meuservidor.com ira procurar em ftp://meuservidor.com/
FTPDir = pasta ftp onde o executavel ficará, por exemplo FTPDir=update quer dizer que o componente ira procurar em FTP://meuservidor.com/update
FTPPassive = se o servidor FTP esta configurado para passivo, onde não exista a necessidade de passar senha.
FTPPassword=Senha do servidor FTP
FTPUser=Usuário do servidor FTP
OptionalUpdate=Se True permite que o usuário não faça naquele momento o update da aplicação.
UpdateMessage=Mensagem que será exibida para o usuário no momento em que o componente verificar que a versão atual não é a ultima.

Principais methodos.

    AutoUpdate1.Execute; // Executa o update 
    AutoUpdate1.Deploy; // Executa o deploy

Principais Eventos
OnNeedDeploy = executador ao verificar que a versão atual é maior que a versão executada no servidor.

Exemplo de codigo.

```
procedure TFORM_PRINCIP.AutoUpdate1NeedDeploy(Sender: TObject);
begin
  if MessageDlg('A versão local é superior à do servidor.'#13 +
    'Deseja fazer upload da versão local para distribuição?',
    mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes then
    AutoUpdate1.Deploy;
end;
```


