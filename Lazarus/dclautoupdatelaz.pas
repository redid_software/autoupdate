{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit dclAutoUpdateLaz;

{$warn 5023 off : no warning about unused units}
interface

uses
  AutoUpdate, unAtualizando, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AutoUpdate', @AutoUpdate.Register);
end;

initialization
  RegisterPackage('dclAutoUpdateLaz', @Register);
end.
