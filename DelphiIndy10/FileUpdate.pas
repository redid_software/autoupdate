unit FileUpdate;

interface

uses
  Classes, SysUtils, IdFTP, IdAntiFreeze, IdComponent, unAtualizando,
  IdFtpCommon, Dialogs, Windows, Controls;

type
  TConfirmEvent = procedure (Sender: TObject; var Stop: boolean) of object;

  {$DEFINE INDY_10}
  TFileUpdate = class(TComponent)
  private
    Client: TIdFTP;
    AntiFreeze: TIdAntiFreeze;
    FFileName: TFileName;
    FFTPDir: string;
    FFTPPassword: string;
    FFTPHost: string;
    FFTPUser: string;
    FLocalRoot: string;
    FFTPPassive: Boolean;
    FBeforeUpdate: TConfirmEvent;
    BytesToTransfer: LongWord;
    FCacheServerVersion: string;
    FOnDeploy: TConfirmEvent;
    procedure SetFileName(const Value: TFileName);
    procedure SetFTPDir(const Value: string);
    procedure SetFTPHost(const Value: string);
    procedure SetFTPPassive(const Value: Boolean);
    procedure SetFTPPassword(const Value: string);
    procedure SetFTPUser(const Value: string);
    procedure SetLocalRoot(const Value: string);
    procedure SetBeforeUpdate(const Value: TConfirmEvent);
    procedure FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      {$IFNDEF INDY_10} const {$ENDIF} AWorkCountMax: Int64);
    procedure FTPWork(Sender: TObject; AWorkMode: TWorkMode;
      {$IFNDEF INDY_10} const {$ENDIF} AWorkCount: Int64);
    procedure SetClientVersao(Versao: string);
    procedure SetOnDeploy(const Value: TConfirmEvent);
  protected
    procedure GetClientInstance;
    function CompareVersions: integer; virtual;
    function GetClientVersion: string; virtual;
    function GetServerVersion: string; virtual;
    procedure Update;
  public
    procedure Execute;
    destructor Destroy; override;
    procedure Deploy;
    function FTPAvailable: boolean;
  published
    property FileName: TFileName read FFileName write SetFileName;
    property LocalRoot: string read FLocalRoot write SetLocalRoot;
    property FTPHost: string read FFTPHost write SetFTPHost;
    property FTPUser: string read FFTPUser write SetFTPUser;
    property FTPPassword: string read FFTPPassword write SetFTPPassword;
    property FTPDir: string read FFTPDir write SetFTPDir;
    property FTPPassive: Boolean read FFTPPassive write SetFTPPassive;
    // Eventos:
    property BeforeUpdate: TConfirmEvent
      read FBeforeUpdate write SetBeforeUpdate;
    property OnDeploy: TConfirmEvent read FOnDeploy write SetOnDeploy;
  end;

  procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Ric', [TFileUpdate]);
end;

var
  frmAtualizando: TfrmAtualizando;

function CompareVersion(UmaVersao, OutraVersao: String): Shortint;
var
  a, b, i, j: byte;
begin
  i := pos('.', UmaVersao);
  if i = 0 then
    a := StrToInt(UmaVersao)
  else
    a := StrToInt(copy(UmaVersao, 1, i-1));

  j := pos('.', OutraVersao);
  if j = 0 then
    b := StrToInt(OutraVersao)
  else
    b := StrToInt(copy(OutraVersao, 1, j-1));

  if a <> b then
  begin
    if a > b then
      result := 1
    else
      result := -1;
  end
  else if i = 0 then
      result := 0
  else
    result := CompareVersion(
      copy(UmaVersao,   i+1, length(UmaVersao)),
      copy(OutraVersao, j+1, length(OutraVersao))
    );
end;

function LoadFirstLine(FileName: TFileName): string;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    sl.LoadFromFile(FileName);
    result := sl[0];
  finally
    sl.Free;
  end;
end;

{ TFileUpdate }

function TFileUpdate.CompareVersions: integer;
var
  ClientVer, ServerVer: string;
begin
  ClientVer := GetClientVersion;
  ServerVer := GetServerVersion;

  result := CompareVersion(ServerVer, ClientVer);
end;

procedure TFileUpdate.Deploy;
var
  LocalName, Versao: string;
  stream: TFileStream;
  memory: TMemoryStream;
  sl: TStringList;
begin
  GetClientInstance;
  LocalName := IncludeTrailingPathDelimiter(LocalRoot) + FileName;
  frmAtualizando := TfrmAtualizando.Create(Self);
  stream := TFileStream.Create(LocalName, fmShareDenyNone);
  memory := TMemoryStream.Create;
  sl := TStringList.Create;
  try
    frmAtualizando.Show;
    frmAtualizando.Message := 'Enviando...';
    stream.Position := 0;
    BytesToTransfer := stream.Size;
    Client.TransferType := ftBinary;
    Client.Put(stream, ExtractFileName(FileName));

    frmAtualizando.Message := 'Atualizando vers�o...';
    versao := GetClientVersion;
    sl.Add(versao);
    sl.SaveToStream(memory);
    memory.Position := 0;
    BytesToTransfer := memory.Size;
    Client.Put(memory, ChangeFileExt(ExtractFileName(FileName), '.ver'));

    Client.Disconnect;
    frmAtualizando.Message := 'Pronto';
  finally
    memory.Free;
    stream.Free;
    FreeAndNil(frmAtualizando);
    sl.Free;
  end;
  ShowMessage('Deploy finalizado.');
end;

destructor TFileUpdate.Destroy;
begin
  if Client <> nil then
  begin
    Client.Free;
    AntiFreeze.Free;
  end;
  inherited;
end;

procedure TFileUpdate.Execute;
var
  i: integer;
  stop: boolean;
begin
  {$WARN SYMBOL_PLATFORM OFF}
  i := CompareVersions;
  if i > 0 then
  begin
    stop := False;
    if Assigned(FBeforeUpdate) then
      FBeforeUpdate(Self, stop);
    if stop then
      exit;

    MessageDlg(
      'H� uma nova vers�o dispon�vel. O arquivo ser� atualizado.',
      mtInformation, [mbOk], 0
    );
    Update;
  end
  else if i < 0 then
  begin
    stop := False;
    if Assigned(FOnDeploy) then
      FOnDeploy(Self, stop);
    if not stop then
      Deploy;
  end;
end;

function TFileUpdate.FTPAvailable: boolean;
begin
  try
    GetClientInstance;
    result := True;
  except;
    result := False;
  end;
end;

procedure TFileUpdate.FTPWork(Sender: TObject; AWorkMode: TWorkMode;
  {$IFNDEF INDY_10} const {$ENDIF} AWorkCount: Int64);
begin
  frmAtualizando.Position := AWorkCount;
end;

procedure TFileUpdate.FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  {$IFNDEF INDY_10} const {$ENDIF} AWorkCountMax: Int64);
begin
  if AWorkCountMax > 0 then
    frmAtualizando.Max := AWorkCountMax
  else
    frmAtualizando.Max := BytesToTransfer;
end;

procedure TFileUpdate.GetClientInstance;
begin
  if Client = nil then
  begin
    if FTPHost = '' then
      raise Exception.Create('FTPHost n�o definido');
    AntiFreeze         := TIdAntiFreeze.Create(Self);
    Client             := TIdFTP.Create(Self);
    Client.OnWorkBegin := FTPWorkBegin;
    Client.OnWork      := FTPWork;
    Client.Host        := FTPHost;
    Client.Username    := FTPUser;
    Client.Password    := FTPPassword;
    Client.Passive     := FTPPassive;
  end;
  if not Client.Connected then
    Client.Connect;
  if not Client.Connected then
    raise Exception.Create('Erro na conex�o com o servidor de FTP');
  Client.ChangeDir(FTPDir);
end;

function TFileUpdate.GetClientVersion: string;
var
  FullName: TFileName;
begin
  FullName := ChangeFileExt(
    IncludeTrailingPathDelimiter(LocalRoot) + FileName,
    '.ver'
  );
  if not FileExists(FullName) then
    SetClientVersao('0.0.0.0');
  result := LoadFirstLine(FullName);
end;

function TFileUpdate.GetServerVersion: string;
var
  st: TMemoryStream;
  sl: TStringList;
begin
  if FCacheServerVersion <> '' then
  begin
    result := FCacheServerVersion;
    exit;
  end;

  GetClientInstance;
  st := TMemoryStream.Create;
  frmAtualizando := TfrmAtualizando.Create(nil);
  sl := TStringList.Create;
  try
    Client.Get(ChangeFileExt(ExtractFileName(FileName), '.ver'), st);
    st.Position := 0;
    sl.LoadFromStream(st);
    result := sl[0];
    FCacheServerVersion := result;
    Client.Disconnect;
  finally
    st.Free;
    frmAtualizando.Free;
    sl.Free;
  end;
end;

procedure TFileUpdate.SetBeforeUpdate(const Value: TConfirmEvent);
begin
  FBeforeUpdate := Value;
end;

procedure TFileUpdate.SetClientVersao(Versao: string);
var
  FullName: TFileName;
  sl: TStringList;
begin
  FullName := ChangeFileExt(
    IncludeTrailingPathDelimiter(LocalRoot) + FileName,
    '.ver'
  );
  sl := TStringList.Create;
  try
    sl.Add(Versao);
    sl.SaveToFile(FullName);
  finally
    sl.Free;
  end;
end;

procedure TFileUpdate.SetFileName(const Value: TFileName);
begin
  if Value <> FFileName then
  begin
    FFileName := Value;
    FCacheServerVersion := '';
  end;
end;

procedure TFileUpdate.SetFTPDir(const Value: string);
begin
  FFTPDir := Value;
end;

procedure TFileUpdate.SetFTPHost(const Value: string);
begin
  FFTPHost := Value;
end;

procedure TFileUpdate.SetFTPPassive(const Value: Boolean);
begin
  FFTPPassive := Value;
end;

procedure TFileUpdate.SetFTPPassword(const Value: string);
begin
  FFTPPassword := Value;
end;

procedure TFileUpdate.SetFTPUser(const Value: string);
begin
  FFTPUser := Value;
end;

procedure TFileUpdate.SetLocalRoot(const Value: string);
begin
  FLocalRoot := Value;
end;

procedure TFileUpdate.SetOnDeploy(const Value: TConfirmEvent);
begin
  FOnDeploy := Value;
end;

procedure TFileUpdate.Update;
var
  source, dest: string;
begin
  GetClientInstance;
  frmAtualizando := TfrmAtualizando.Create(Self);
  try
    Client.TransferType := ftBinary;
    source := ExtractFileName(FileName);
    BytesToTransfer := Client.Size(source);
    frmAtualizando.Show;
    frmAtualizando.Message := 'Obtendo nova vers�o...';
    dest := IncludeTrailingPathDelimiter(LocalRoot) + FileName;
    if FileExists(ChangeFileExt(dest, '.bak')) then
      DeleteFile(pchar(ChangeFileExt(dest, '.bak')));
    if FileExists(dest) then
      RenameFile(dest, ChangeFileExt(dest, '.bak'));
    Client.Get(source, dest, true);

    frmAtualizando.Message := 'Atualizando informa��es de vers�o...';
    SetClientVersao(GetServerVersion);

    Client.Disconnect;
    frmAtualizando.Message := 'Pronto!';
  finally
    frmAtualizando.Free;
  end;
end;

end.
